#!/usr/bin/env node

const commander = require('commander');
const inquirer = require('inquirer');
inquirer.registerPrompt('jsonFile', require('inquirer-parse-json-file'));
const shell = require('shelljs');
const colors = require('colors');
const replace = require('replace-in-file');
const globby = require('globby');
const ora = require('ora');
const del = require('del');
const fs = require('fs');
const path = require('path');
const Table = require('cli-table');
const replaceLast = require('replace-last');

//const fs = require('fs');
let spinner;

const package = require('./package.json');

commander.version(package.version);

// Main command
commander
	.version(package.version)
	.arguments('<name>')
	.description('Creates the foundation for a project using a predefined structure')
	.option('-d, --dry', 'Log the possible changes without making them')
	.action(async (name) => {
		// Check if Git is installed
		if (!shell.which('git')) {
			shell.echo('Sorry, this script requires git. Make sure you have git installed before running this command.');
			shell.exit(1);
		}

		// Get info for each available boilerplate
		const availableBoilerplates = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'boilerplates.json')));

		// Request remaining info
		console.log('');
		const { boilerplate, json } = await inquirer.prompt([
			{
				type: 'list',
				name: 'boilerplate',
				message: `Choose a boilerplate?`,
				choices: () => {
					const choices = [];
					for (const key in availableBoilerplates) {
						if (availableBoilerplates.hasOwnProperty(key)) {
							choices.push({
								name: availableBoilerplates[key].label,
								value: key,
							});
						}
					}
					return choices;
				},
			},
			{
				type: 'input',
				name: 'json',
				message: `Paste the project info as minified JSON data:`,
				validate: (input) => {
					let json = {};
					try {
						json = JSON.parse(input);
					} catch (err) {
						return (
							`\n! The provided data doesn't seem to be a valid JSON.`.red +
							`\n  Make sure the JSON is minified and in one single line.`.red
						);
					}
					if (!json.hasOwnProperty('title')) {
						return `\n! The title property must exist in the JSON data.`.red;
					}
					return true;
				},
			},
		]);

		const data = JSON.parse(json);
		data.folder = name;

		// Confirm project details
		const { confirmDetails } = await inquirer.prompt({
			type: 'confirm',
			name: 'confirmDetails',
			message: `Continue setup of ${colors.green(data.title)}?`,
		});

		if (!confirmDetails) {
			console.log(`\n! Ok, nothing changed.\n`);
			return;
		}

		try {
			// Clone the repository
			await cloneSourceProject(availableBoilerplates[boilerplate], name);

			// Get configurations from the downloaded boilerplate
			const config = require(`${process.cwd()}/${name}/boilerconfig`)(data);

			// Replace tokens on project files
			await replaceTokens(config, name);

			// Rename files and folders
			await renamePaths(config, name);

			// Delete Git repository from new project
			await cleanFiles(config, name);

			console.log(colors.green('✔ Project created successfully!\n'));
		} catch (err) {
			console.log(err);
			console.log(
				colors.red(
					`Sorry, it seems that a problem occurred during the process. See the logs above for more details.\n`
				)
			);
		}
	});

/**
 * Clone the respective project for the chosen Stack
 * @param {object} context Data from the choosed project type
 * @param {object} folder Information collected for the project
 */
function cloneSourceProject(context, folder) {
	return new Promise((resolve, reject) => {
		// Start spinner with basic message
		const spinnerText = `Cloning project structure ${colors.gray(
			"(This may take a while depending on Gitlab's availability)"
		)}`;
		let spinner = ora({ text: spinnerText, color: 'green' });
		spinner.start();

		// Clones the bootstrap project
		const gitClone = shell.exec(`git clone ${context.repository} ${folder}`, { silent: true });
		if (!gitClone.code) {
			spinner.succeed(colors.green(spinnerText));
			spinner.clear();
			resolve();
		} else {
			spinner.fail(colors.red(spinnerText));
			spinner.clear();
			reject('Unable to clone the repository. Make sure you have the read right to clone the source repository.');
		}
	});
}

/**
 * Replace tokens in the project files using the provided info
 * @param {object} config Replacement settings from the boilerplate
 * @param {string} folder Name of the root folder of the new project
 */
function replaceTokens(config, folder) {
	return new Promise(async (resolve, reject) => {
		const isDryEnabled = commander.opts().dry;

		// Add spinner message
		const replacingText = 'Replacing tokens on files';
		spinner = ora({ text: replacingText, color: 'green' });
		spinner.start();

		try {
			// Run each replacement entry with the providen options
			const table = new Table({
				head: ['Rule'.cyan, 'File'.cyan, 'Occurrences found'.cyan],
			});
			let totalReplacements = 0;
			for (const [index, options] of config.replace.entries()) {
				options.files = options.files.map((path) => getRelativePath(folder, path)); // TODO: Add support for single string type
				options.ignore = options.hasOwnProperty('ignore') // TODO: Add support for single string type
					? options.ignore.map((path) => getRelativePath(folder, path))
					: [];
				options.countMatches = true;
				// Do not make the replacements if on dry mode
				if (isDryEnabled) {
					options.dry = true;
				}
				const results = await replace(options);
				results.forEach((file) => {
					if (isDryEnabled) {
						if (!file.numReplacements) {
							return;
						}
						table.push([
							colors.gray(`[${colors.green(index)}]`),
							getHighlightedPath(file.file),
							file.numReplacements.toString().yellow,
						]);
					} else {
						totalReplacements += file.numReplacements;
					}
				});
			}

			const subject = totalReplacements === 1 ? 'occurrence' : 'occurrences';
			const infoText = isDryEnabled ? `${subject} to replace` : `${subject} replaced`;
			spinner.succeed(colors.green(replacingText) + colors.gray(` - ${totalReplacements} ${infoText}`));
			spinner.clear();

			if (isDryEnabled) {
				// Print table with possible replacements
				console.log(table.toString());
			}

			resolve(true);
		} catch (err) {
			spinner.fail(colors.red(replacingText));
			spinner.clear();
			reject(err);
		}
	});
}

/**
 * Rename files and folders in the project using the provided info
 * @param {object} config Replacement settings from the boilerplate
 * @param {string} folder Name of the root folder of the new project
 */
function renamePaths(config, folder) {
	return new Promise(async (resolve, reject) => {
		const isDryEnabled = commander.opts().dry;

		// Add spinner message
		const spinnerText = 'Renaming files and folders';
		spinner = ora({ text: spinnerText, color: 'green' });
		spinner.start();

		try {
			const table = new Table({
				head: ['Rule'.cyan, 'From'.cyan, 'To'.cyan],
			});
			let totalRenamed = 0;
			// Get paths and files from globs and rename
			for (const [index, rename] of config.rename.entries()) {
				const paths = await globby(
					Array.isArray(rename.paths)
						? rename.paths.map((glob) => getRelativePath(folder, glob)) // TODO: Add support for single string type
						: getRelativePath(folder, rename.paths),
					{ expandDirectories: false, onlyFiles: false }
				);
				paths.forEach((path) => {
					const subpaths = path.split('/');
					const last = subpaths.pop();
					if (!last.includes(rename.from)) {
						return;
					}
					const renamedPath = subpaths.join('/') + '/' + replaceLast(last, rename.from, rename.to);
					// Do not make the renaming if on dry mode
					if (isDryEnabled) {
						table.push([
							colors.gray(`[${colors.green(index)}]`),
							getHighlightedPath(path, rename.from),
							getHighlightedPath(renamedPath.split('/').pop(), rename.to, true),
						]);
					} else {
						totalRenamed++;
						fs.renameSync(path, renamedPath);
					}
				});
			}

			const subject = totalRenamed === 1 ? 'entity' : 'entities';
			const infoText = isDryEnabled ? `${subject} to rename` : `${subject} renamed`;
			spinner.succeed(colors.green(spinnerText) + colors.gray(` - ${totalRenamed} ${infoText}`));
			spinner.clear();

			if (isDryEnabled) {
				// Print table with possible renaming
				console.log(table.toString());
			}

			resolve();
		} catch (err) {
			spinner.fail(colors.red(spinnerText));
			spinner.clear();
			reject(err);
		}
	});
}

function getHighlightedPath(path, highlight, clamp = false) {
	const subpaths = path.split('/');
	let last = subpaths.pop();
	last = highlight ? last.split(highlight).join(highlight.bold.yellow) : last;
	return colors.gray(clamp ? '…/' : subpaths.join('/') + '/') + last;
}

function getRelativePath(folder, path) {
	return `${process.cwd()}/${folder}/${path}`;
}

/**
 * Delete unnecessary files from the new cloned repository
 * @param {object} config Deletion settings from the boilerplate
 * @param {string} folder Name of the root folder of the new project
 */
function cleanFiles(config, folder) {
	return new Promise((resolve, reject) => {
		// Add spinner message
		const spinnerText = 'Cleaning files';
		spinner = ora({ text: spinnerText, color: 'green' });
		spinner.start();

		// Delete Git Repository
		try {
			del([`${folder}/.git`]);

			spinner.succeed(colors.green(spinnerText));
			spinner.clear();
			resolve();
		} catch (err) {
			spinner.fail(colors.red(spinnerText));
			spinner.clear();
			reject(err);
		}
	});
}

commander.parse(process.argv);

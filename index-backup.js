#!/usr/bin/env node

const commander	= require('commander');
const inquirer	= require('inquirer');
const shell		= require('shelljs');
const colors	= require('colors');
const replace	= require('replace-in-file');
const ora		= require('ora');
const del		= require('del');
const fs		= require('fs');
const path		= require('path');

//const fs = require('fs');
let spinner;

const package = require('./package.json');

commander.version(package.version);

// UPDATE command
commander
	.command('update')
	.description('Pega a última versão da CLI, pelo Git')
	.action(async () => {
		console.log('');
		shell.cd(__dirname);
	});

// CREATE command
commander
    .command('create <name>')
	.description('Creates the foundation for a project using a predefined structure')
    .action(async (name) => {

		// Check if Git is installed
		if (!shell.which('git')) {
			shell.echo('Sorry, this script requires git. Make sure you have git installed before running this command.');
			shell.exit(1);
		}

		// Get info for each type of project
		const projectTypes = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'projects.json')));

		// Request remaining info
		console.log('');
		let projectAnswers = await inquirer.prompt([
			{
				type: 'list',
				name: 'projectType',
				message: `What's the project type?`,
				choices: () => {
					const choices = [];
					for (const key in projectTypes) {
						if (projectTypes.hasOwnProperty(key)) {
							choices.push({
								name: projectTypes[key].label,
								value: key,
							})
						}
					}
					return choices;
				}
			},
			{
				type: 'input',
				name: 'fullName',
				message: `What's the official name of the project? ${colors.gray('(i.e.: Star Wars)')}`,
				validate: value => value ? true : 'This field is mandatory.'
			},
			{
				type: 'input',
				name: 'authorName',
				message: `What's the author name? ${colors.gray('(i.e.: Lucasfilm)')}`,
				validate: value => value ? true : 'This field is mandatory.'
			},
			{
				type: 'input',
				name: 'authorSite',
				message: `What's the author site URI (if available)? ${colors.gray('(i.e.: https://www.lucasfilm.com/)')}`,
				validate: value => {
					if(!value) {
						return true;
					} else if(!/\w+:(\/?\/?)[^\s]+/.test(value)) {
						return "Please enter a valid URI, like http://www.example.com/ or leave it empty.";
					} else {
						return true;
					}
				}
			},
			{
				type: 'input',
				name: 'clientName',
				message: `What's the name of the client? ${colors.gray('(i.e.: Disney)')}`,
				validate: value => value ? true : 'This field is mandatory.'
			},
		]);

		const projectInfo = {
			folderName: name,
			fullName: projectAnswers.fullName.trim(),
			packageName: packageName( projectAnswers.fullName ),
			authorName: projectAnswers.authorName.trim(),
			authorSite: projectAnswers.authorSite.trim(),
			clientName: projectAnswers.clientName.trim(),
			type: projectAnswers.projectType,
		};

		/* const projectInfo = {
			folderName: name,
			fullName: 'Captalys',
			packageName: packageName( 'Captalys' ),
			authorName: 'Green Park Content',
			authorSite: 'https://www.greenparkcontent.com/',
			clientName: 'Captalys',
			themeName: 'captalys',
			type: projectAnswers.projectType,
		}; */
		
		// If WordPress project, ask for theme name
		if( projectInfo.type === 'wordpress' || projectInfo.type === 'wordpress_headless' ) {
			let { themeName } = await inquirer.prompt([
				{
					type: 'input',
					name: 'themeName',
					message: `What's the name of the theme folder: ${colors.gray('(i.e.: starwars or sw)')}`,
					validate: value => {
						if(!/^[a-z][a-z0-9_]*$/.test(value)) {
							return "Must start with a letter, and can't have spaces, dashes, accents, special chars or uppercase letters (i.e. twentytwenty, number9, another_one).";
						} else {
							return true;
						}
					}
				}
			]);
			projectInfo.themeName = themeName;
		}

		// Print project details
		console.log(`
${colors.yellow('! The project will be created with the following info:')}
- Project Name: ${colors.green(projectInfo.fullName)}
- Package:      ${colors.gray(projectInfo.packageName)}
- Root Folder:  ${colors.green(projectInfo.folderName)}
- Local Domain: ${colors.gray(`http://${projectInfo.folderName}.local/`)}
- Database:     ${colors.gray(`${projectInfo.folderName}_local`)}
- Theme Name:   ${colors.green(projectInfo.themeName)}
- Text Domain:  ${colors.gray(`${projectInfo.themeName} (i.e. __('Text', '${projectInfo.themeName}'))`)}
- Author:       ${colors.green(`${projectInfo.authorName + (projectInfo.authorSite ? ` - ${projectInfo.authorSite}` : '')}`)}
- Client:       ${colors.green(projectInfo.clientName)}
- Project:      ${colors.green(projectTypes[projectInfo.type].label)}
`);

		// Confirm project details
		const { confirmDetails } = await inquirer.prompt({
			type: 'confirm',
			name: 'confirmDetails',
			message: `Do you want to continue?}`,
		});

		if(!confirmDetails) {
			console.log(`\n${colors.yellow('! Ok, nothing changed.')}\n! Run the task again with the correct information.\n`);
			return;
		}

		try {
			// Clone the repository
			await cloneSourceProject(projectTypes[projectInfo.type], projectInfo);

			// Replace terms on project files
			await replaceThemeTerms(projectTypes[projectInfo.type], projectInfo);
			
			// Rename files and folders
			await renameFiles(projectTypes[projectInfo.type], projectInfo);

			// Delete Git repository from new project
			await cleanFiles(projectTypes[projectInfo.type], projectInfo);
	
			console.log(colors.green('✔ Project created successfully!\n'));
			
		} catch(err) {
			console.log(err);
			console.log(colors.red(`Sorry, it seems that a problem occurred during the process. See the logs above for more details.\n`));
		}

	});

/**
 * Return package string for a project name
 * @param {string} str Name of the project
 */
function packageName(str) {
	let pkg = str;

	// Remove accents, swap ñ for n, etc
	var from = 'àáäâèéëêìíïîòóöôùúüûñçÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛÑÇ';
	var to = 'aaaaeeeeiiiioooouuuuncAAAAEEEEIIIIOOOOUUUUNC';
	for (var i = 0, l = from.length; i < l; i++) {
		pkg = pkg.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	}

	// Remove invalid chars and convert to PascalCase
	pkg = pkg.replace(/[_-]/g, ' ').replace(/[^a-zA-Z0-9\s]/g, '');
	pkg = toPascalCase( pkg );

	return pkg;
}

/**
 * Change the case format to PascalCase
 * @param {string} str String to be formatted
 */
function toPascalCase(str) {
	let splitStr = str.split(' ');

	// Convert each word to Title case
	splitStr.forEach((word, i) => {
		splitStr[i] = word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();;
	});

	return splitStr.join('');
}

/**
 * Replace
 * @param {string} str String to search for the term to be replaced
 * @param {object} info Default object with project info
 */
function replaceTokens(str, info) {
	str = str
		.replace(/FOLDER_NAME/g, info.folderName)
		.replace(/THEME_NAME/g, info.themeName)
		.replace(/FULL_NAME/g, info.fullName)
		.replace(/PACKAGE_NAME/g, info.packageName)
		.replace(/AUTHOR_NAME/g, info.authorName)
		.replace(/AUTHOR_SITE/g, info.authorSite)
		.replace(/CLIENT_NAME/g, info.clientName);

	return str;
}

/**
 * Clone the respective project for the chosen Stack
 * @param {object} context Data from the choosed project type
 * @param {object} info Information collected for the project
 */
function cloneSourceProject(context, info) {
	return new Promise((resolve, reject) => {

		// Start spinner with basic message
		const spinnerText = `Cloning project structure ${colors.gray('(This may take a while depending on Gitlab\'s availability)')}`;
		let spinner = ora({ text: spinnerText, color: 'green' });
		spinner.start();

		// Clones the bootstrap project
		const gitClone = shell.exec(`git clone ${context.repository} ${info.folderName}`, { silent: true });
		if(!gitClone.code) {
			spinner.succeed(colors.green(spinnerText));
			spinner.clear();
			resolve();
		} else {
			spinner.fail(colors.red(spinnerText));
			spinner.clear();
			reject('Unable to clone the repository. Make sure you have the read right to clone the source repository.');
		}
	});
}

/**
 * Replace terms in the project files using the provided info
 * @param {object} context Data from the choosed project type
 * @param {object} info Information collected for the project
 */
function replaceThemeTerms(context, info) {
	return new Promise((resolve, reject) => {

		// Add spinner message
		const replacingText = 'Replacing terms on files';
		spinner = ora({ text: replacingText, color: 'green' });
		spinner.start();

		// Define the replace rules
		const options = {
			files: context.files.map(glob => `./${info.folderName}/${glob}`),
			ignore: [
				`./${info.folderName}/**/*.eps`,
				`./${info.folderName}/**/*.woff`,
				`./${info.folderName}/**/*.woff2`,
				`./${info.folderName}/**/*.ttf`,
				`./${info.folderName}/**/*.otf`
			],
			from: context.replaces.map(pattern => new RegExp(pattern.from, 'g')),
			to: context.replaces.map(pattern => replaceTokens(pattern.to, info)),
			countMatches: true
		}

		// Run the replace tasks through all project files
		replace(options)
			.then(res => {
				var replacedCount = 0;
				res.forEach(file => {
					replacedCount += file.numReplacements;
				});
				// Success
				spinner.succeed(colors.green(replacingText) + colors.gray(` - ${replacedCount} occurrencies`));
				spinner.clear();

				// console.dir(res.filter(file => file.numReplacements > 0))

				resolve(res);
			})
			.catch(err => {
				// Fail
				spinner.fail(colors.red(replacingText));
				spinner.clear();
				
				reject(err);
			});
	});
}

/**
 * Rename some files from the original structure using the provided project info
 * @param {object} context Data from the choosed project type
 * @param {object} info Information collected for the project
 */
function renameFiles(context, info) {
	return new Promise((resolve, reject) => {

		// Add spinner message
		const spinnerText = 'Renaming files and folders';
		spinner = ora({ text: spinnerText, color: 'green' });
		spinner.start();

		// Rename files
		try {
			context.rename.forEach(rename => {
				fs.renameSync(
					`./${info.folderName}/${rename.from}`,
					replaceTokens(`./${info.folderName}/${rename.to}`, info)
				);
			});

			spinner.succeed(colors.green(spinnerText));
			spinner.clear();
			resolve();

		} catch(err) {
			spinner.fail(colors.red(spinnerText));
			spinner.clear();
			reject(err);
		}
	});
}

/**
 * Delete unnecessary files from the new cloned repository
 * @param {object} context Data from the choosed project type
 * @param {object} info Information collected for the project
 */
function cleanFiles(context, info) {
	return new Promise((resolve, reject) => {

		// Add spinner message
		const spinnerText = 'Cleaning files';
		spinner = ora({ text: spinnerText, color: 'green' });
		spinner.start();

		// Delete Git Repository
		try {
			del([`${info.folderName}/.git`]);

			spinner.succeed(colors.green(spinnerText));
			spinner.clear();
			resolve();

		} catch(err) {
			spinner.fail(colors.red(spinnerText));
			spinner.clear();
			reject(err);
		}
	});
}

commander.parse(process.argv);
# Boiler CLI

A CLI for initializing new projects using boilerplate repositories as the base structure.

## Installation

1. Clone this repository in your machine;
2. Navigate to the project root folder in your terminal;
3. Install dependencies running `npm install`;
4. Run `npm link` to enable the `boiler` command globally;

## Getting Started

For creating a new project, navigate to the directory where you want it to be created, and run the command below:

```shell
boiler <foldername>
```

> Replace `<foldername>` with the name you want for the root folder of the project, usually the name of your project. It **MUST** match the name of the repository where you will host it.

## Roadmap

-  Add support to `add`, `move`, `delete` and `run` (for script) actions;
-  Describe `boilerconfig` configuration and options;
-  Add support for defining the boilerplates separately, with a local file or JSON url;
-  Remove `boilerconfig` file after complete;
-  Add **JSON schema vaidator** with TypeScript;
-  Add option to initialize **Git** with an initial commit;
